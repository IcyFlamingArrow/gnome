# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2018 Rasmus Thomsen <cogitri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ] gsettings
require systemd-service
require meson

SUMMARY="VFS interfaces for GTK+"
HOMEPAGE="https://www.gnome.org/"

LICENCES="LGPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    afc [[ description = [ add support for accessing AFC (Apple File Connection) filesystems ] ]]
    afp [[ description = [ add support for accessing AFP (Apple Filing Protocol) shares ] ]]
    archive [[ description = [ add support for reading compressed archives supported by libarchive ] ]]
    avahi
    bluray [[ description = [ Blu-ray disc metadata support ] ]]
    cdda
    gphoto2
    fuse
    http [[ description = [ enable support http, dav, ssl uris ] ]]
    keyring
    mtp [[ description = [ build MTP volume monitor (e.g. Android tablets and phones) ] ]]
    nfs [[ description = [ add support for accessing NFS shares ] ]]
    online-accounts [[ description = [ build online accounts volume monitor ] ]]
    samba
    udisks [[ description = [ add support for udisks based volume monitoring ] ]]
    ( providers: elogind systemd ) [[
        *description = [ Session tracking provider ]
        number-selected = at-most-one
    ]]
"

# Seems like gnome-disk-utility doesn't offer libgdu in recent versions
# monitor? ( dev-libs/expat
# gnome-desktop/gnome-disk-utility[>=3.0.2] )
DEPENDENCIES="
    build:
        dev-libs/libxslt
        sys-devel/gettext[>=0.19.4]
        virtual/pkg-config[>=0.20]
    build+run:
        dev-libs/glib:2[>=2.65.1]
        dev-libs/libusb:1[>=1.0.21]
        dev-libs/libxml2:2.0
        gnome-desktop/gcr
        gnome-desktop/gsettings-desktop-schemas[>=3.33]
        gnome-desktop/libgudev[>=147]
        net-misc/openssh
        sys-apps/dbus
        sys-auth/polkit[>=0.114]
        sys-libs/libcap
        afc? ( app-pda/libimobiledevice:1.0[>=1.2]
               dev-libs/libplist:2.0[>=0.15] )
        afp? ( dev-libs/libgcrypt[>=1.2.2] )
        archive? ( app-arch/libarchive )
        avahi? ( net-dns/avahi[>=0.6][dbus] )
        bluray? ( media-libs/libbluray )
        cdda? ( dev-libs/libcdio[>=0.78.2]
                dev-libs/libcdio-paranoia )
        fuse? ( sys-fs/fuse:3[>=3.0.0] )
        gphoto2? ( media-libs/libgphoto2[>=2.4.0] )
        http? ( gnome-desktop/libsoup:2.4[>=2.58.0] )
        keyring? ( dev-libs/libsecret:1 )
        mtp? ( media-libs/libmtp[>=1.1.12] [[ note = [ permit on-device reading ] ]] )
        nfs? ( net-fs/libnfs[>=1.9.8] )
        online-accounts? (
            gnome-desktop/gnome-online-accounts[>=3.17.1]
            gnome-desktop/libgdata[>=0.18.0][online-accounts]
        )
        providers:elogind? ( sys-auth/elogind[>=229] )
        providers:systemd? ( sys-apps/systemd[>=206] )
        samba? ( net-fs/samba[>=3.4.0] )
        udisks? ( sys-apps/udisks:2[>=1.97] )
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dadmin=true
    -Dgcr=true
    -Dgudev=true
    -Dinstalled_tests=false
    -Dlibusb=true
    -Dman=true
    -Dsystemduserunitdir="${SYSTEMDUSERUNITDIR}"
    -Dtmpfilesdir="${SYSTEMDTMPFILESDIR}"
    # Overridden below
    -Dlogind=false
)

MESON_SRC_CONFIGURE_OPTIONS=(
    'providers:elogind -Dlogind=true'
    'providers:systemd -Dlogind=true'
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    afc
    afp
    archive
    'avahi dnssd'
    bluray
    cdda
    fuse
    gphoto2
    http
    keyring
    mtp
    nfs
    'online-accounts goa'
    'online-accounts google'
    'samba smb'
    'udisks udisks2'
)

src_compile() {
    # Parallel builds break compilation. See:
    # https://bugzilla.gnome.org/show_bug.cgi?id=789877
    # Once the following PR is merged we can remove this:
    # https://github.com/mesonbuild/meson/pull/2930
    edo ninja -v -j1
}

src_install() {
    meson_src_install

    keepdir /usr/share/gvfs/remote-volume-monitors
}

pkg_preinst() {
    # We formerly had a keepdir on /media, and systemd mounts it as a tmpfs, so we cannot let gvfs
    # remove it and recreate it in postinst or the "rmdir /media" in the merge phase would fail.
    # Create a temporary file so that /media doesn't get unmerged, and remove it in postinst
    edo touch "${ROOT}"/media/.keep-the-media-directory-please
}

pkg_postinst() {
    gsettings_pkg_postinst
    nonfatal edo rm "${ROOT}"/media/.keep-the-media-directory-please
}

