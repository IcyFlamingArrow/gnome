# Copyright 2009 Jonathan Dahan <jedahan@gmail.com>
# Copyright 2011 Brett Witherspoon <spoonb@exherbo.org>
# Copyright 2015 Volodymyr Medvid <vmedvid@riseup.net>
# Copyright 2020 Calvin Walton <calvin.walton@kepstin.ca>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]
require gsettings
require python [ blacklist=2 multibuild=false ]
require vala [ vala_dep=true ]
require meson

SUMMARY="Additional plugins for gedit"
DESCRIPTION="
The gedit-plugins package contains useful plugins that are (most
of the time) too specific to be distributed with gedit itself.

Bookmarks: Easy document navigation with bookmarks
Bracket Completion: Automatically adds closing brackets.
Character Map: Insert special characters just by clicking on them.
Code Comment: Comment out or uncomment a selected block of code.
Color Picker: Pick a color from a dialog and insert its hexadecimal representation.
Color Scheme Editor: Source code color scheme editor
Commander: Command line interface for advanced editing
Draw Spaces: Draw spaces and tabs
Embedded Terminal: Embed a terminal in the bottom pane.
Find in Files: Find text in all files of a folder.
Git: Highlight lines that have been changed since the last commit
Join/Split Lines: Join several lines or split long ones
Multi Edit: Edit document in multiple places at once
Session Saver: Save and restore your working sessions
Smart Spaces: Forget you’re not using tabulations.
SyncTeX: Synchronize between LaTeX and PDF with gedit and evince.
Text Size: Easily increase and decrease the text size
Translate: Translates text into different languages
Word Completion: Word completion using the completion framework
"
HOMEPAGE="https://live.gnome.org/Gedit/Plugins"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    charmap     [[ description = [ Enable the Character Map plugin ] ]]
    git         [[ description = [ Enable the Git plugin ] ]]
    synctex     [[ description = [ Enable the SyncTeX plugin ] ]]
    terminal    [[ description = [ Enable the Embedded Terminal plugin ] ]]
"

DEPENDENCIES="
    build:
        dev-libs/appstream-glib
        gnome-desktop/yelp-tools
        sys-devel/gettext
        virtual/pkg-config
    build+run:
        app-editors/gedit[>=3.38.0]
        dev-libs/glib:2
        dev-libs/libpeas:1.0[>=1.14.1][python_abis:*(-)?]
        gnome-bindings/pygobject:3[cairo][python_abis:*(-)?]
        gnome-desktop/gtksourceview:4.0[gobject-introspection]
        x11-libs/gdk-pixbuf:2.0[gobject-introspection]
        x11-libs/gtk+:3[gobject-introspection]
        x11-libs/pango[gobject-introspection]
        charmap? ( gnome-extra/gucharmap[gobject-introspection] )
        git? ( dev-scm/libgit2-glib:1.0[gobject-introspection] )
        synctex? ( dev-python/dbus-python[>=0.82][python_abis:*(-)?] )
        terminal? ( dev-libs/vte:2.91[gobject-introspection] )
    run:
        synctex? ( gnome-desktop/evince )
"

# Enable all python and C plugins with common dependencies by default
MESON_SRC_CONFIGURE_PARAMS=(
    -Dplugin_bookmarks=true
    -Dplugin_bracketcompletion=true
    -Dplugin_codecomment=true
    -Dplugin_colorpicker=true
    -Dplugin_colorschemer=true
    -Dplugin_commander=true
    -Dplugin_drawspaces=true
    -Dplugin_findinfiles=true
    -Dplugin_joinlines=true
    -Dplugin_multiedit=true
    -Dplugin_sessionsaver=true
    -Dplugin_smartspaces=true
    -Dplugin_textsize=true
    -Dplugin_translate=true
    -Dplugin_wordcompletion=true
)
# Plugins that add extra dependencies
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'charmap plugin_charmap'
    'git plugin_git'
    'synctex plugin_synctex'
    'terminal plugin_terminal'
)

