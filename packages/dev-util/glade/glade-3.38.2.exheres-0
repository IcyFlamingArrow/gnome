# Copyright 2009 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]
require python [ blacklist=none with_opt='true' multibuild=false ]
require gsettings gtk-icon-cache freedesktop-desktop freedesktop-mime meson

SUMMARY="GTK+ UI Designer"
HOMEPAGE="https://glade.gnome.org"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="gobject-introspection gtk-doc python
    webkit [[ description = [ build webkit2gtk catalog ] ]]
"

DEPENDENCIES="
    build:
        dev-libs/libxslt [[ note = [ for manpages ] ]]
        dev-util/intltool[>=0.41.0]
        gnome-desktop/yelp-tools
        sys-devel/gettext
        virtual/pkg-config[>=0.20]
        gtk-doc? ( dev-doc/gtk-doc[>=1.13] )
    build+run:
        dev-libs/glib:2[>=2.64.0]
        dev-libs/libxml2:2.0[>=2.4.0]
        x11-libs/gtk+:3[>=3.24.0]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=1.32.0] )
        python? ( gnome-bindings/pygobject:3[>=3.8.0][python_abis:*(-)?] )
        webkit? ( net-libs/webkit:4.0[>=2.28] )
"

# Require X11
RESTRICT="test"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dgladeui=true
    -Dgjs=disabled
    -Dmac_bundle=false
    -Dman=true
)
MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    'python'
    'webkit webkit2gtk'
)
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'gobject-introspection introspection'
    'gtk-doc gtk_doc'
)

src_install() {
    meson_src_install

    edo find "${IMAGE}" -type d -empty -delete
}

pkg_postinst() {
    gsettings_pkg_postinst
    gtk-icon-cache_pkg_postinst
    freedesktop-desktop_pkg_postinst
    freedesktop-mime_pkg_postinst
}

pkg_postrm() {
    gsettings_pkg_postrm
    gtk-icon-cache_pkg_postrm
    freedesktop-desktop_pkg_postrm
    freedesktop-mime_pkg_postrm
}

